from selenium import webdriver
import unittest
driver = webdriver.Firefox(executable_path=r'D:\geckodriver.exe')
class NewVisitorTest(unittest.TestCase):
    '''Тест нового посетителя'''
    def setUp(self):
        '''установка'''
        self.browser=webdriver.Firefox()
    def tearDown(self):
        '''демонтаж'''
        self.browser.quit()
    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://127.0.0.1:8000/')
if __name__=='__main__':
    unittest.main(warnings='ignore')